// React Native Geolocation
// https://aboutreact.com/react-native-geolocation/
// import React in our code

import React, {useState, useEffect} from 'react';

// import all the components we are going to use

import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  Image,
  PermissionsAndroid,
  Platform,
  Button,
} from 'react-native';

//import all the components we are going to use.

//const Geolocation_lib = 'react-native-geolocation-service' 
const Geolocation_lib = '@react-native-community/geolocation';

import Geolocation from '@react-native-community/geolocation';

const App = () => {
  const [
    currentLongitude,
    setCurrentLongitude
  ] = useState('...');

  const [
    currentLatitude,
    setCurrentLatitude
  ] = useState('...');

  const [
    currentAccuracy,
    setCurrentAccuracy
  ] = useState('...');

  const [
    locationStatus,
    setLocationStatus
  ] = useState('');

  useEffect(() => {
    const requestLocationPermission = async () => {
      if (Platform.OS === 'ios') {
        getOneTimeLocation();
        subscribeLocationLocation();
      } else {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
            {
              title: 'Location Access Required',
              message: 'This App needs to Access your location',
            },
          );

          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            //To Check, If Permission is granted
            getOneTimeLocation();
            subscribeLocationLocation();
          } else {
            setLocationStatus('Permission Denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    };

    requestLocationPermission();

    return () => {
      Geolocation.clearWatch(watchID);
    };
  }, []);

 

  const getOneTimeLocation = () => {
    setLocationStatus('Getting Location ...');
    Geolocation.getCurrentPosition(
      //Will give you the current location
      (position) => {
        setLocationStatus('You are Here');
        console.log('getOneTimeLocation:');
        console.log(position);
 
        //getting the Longitude from the location json
        const currentLongitude = 
          JSON.stringify(position.coords.longitude);
 
        //getting the Latitude from the location json
        const currentLatitude = 
          JSON.stringify(position.coords.latitude);

        //getting the Accuracy from the location json
        const currentAccuracy = 
          JSON.stringify(position.coords.accuracy);
 
        //Setting Longitude state
        setCurrentLongitude(currentLongitude);
        
        //Setting Longitude state
        setCurrentLatitude(currentLatitude);

        //Setting Accuracy state
        setCurrentAccuracy(currentAccuracy);

      },
      (error) => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: true,
        timeout: 30000,         // At the first time we wait max 30sec for the location
        maximumAge: 0
      },
    );
  };
 
  const subscribeLocationLocation = () => {
    watchID = Geolocation.watchPosition(
      (position) => {
        //Will give you the location on location change
        
        setLocationStatus('You are Here');
        console.log('subscribeLocation:');
        console.log(position);
 
        //getting the Longitude from the location json        
        const currentLongitude =
          JSON.stringify(position.coords.longitude);
 
        //getting the Latitude from the location json
        const currentLatitude = 
          JSON.stringify(position.coords.latitude);

        //getting the Accuracy from the location json
        const currentAccuracy = 
          JSON.stringify(position.coords.accuracy);
 
        //Setting Longitude state
        setCurrentLongitude(currentLongitude);
 
        //Setting Latitude state
        setCurrentLatitude(currentLatitude);

        //Setting Accuracy state
        setCurrentAccuracy(currentAccuracy);
      },
      (error) => {
        setLocationStatus(error.message);
      },
      {
        enableHighAccuracy: true,
        maximumAge: 1000, // Within 1sec not getting new location even if distance over 1m
        //timeout: INFINITY as default
        timeout: 5000, //After 1m distance or 1sec waiting 5sec waiting for getting a new location from the GPS
        distanceFilter: 1,
        //useSignificantChanges: true   //iOS
        //forceLocationManager: true    //Android
      },
    );
  };
 
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
          <Text style={styles.smallText}>
            {Geolocation_lib}
          </Text>
        <View style={styles.container}>
          <Image
            source={{
              uri:
                'https://raw.githubusercontent.com/AboutReact/sampleresource/master/location.png',
            }}
            style={{width: 100, height: 100}}
          />
          <Text style={styles.boldText}>
            {locationStatus}
          </Text>
          <Text style={styles.Text}>
            Longitude: {currentLongitude}
          </Text>
          <Text style={styles.Text}>
            Latitude: {currentLatitude}
          </Text>
          <Text style={styles.Text}>
            Accuracy: {currentAccuracy}
          </Text>
          <View style={{marginTop: 20}}>
            <Button
              title="Button"
              onPress={getOneTimeLocation}
            />
          </View>
        </View>
        <Text
          style={{
            fontSize: 18,
            textAlign: 'center',
            color: 'grey'
          }}>
          OBM vs React Native Geolocation
        </Text>
      </View>
    </SafeAreaView>
  );
};
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  boldText: {
    fontSize: 25,
    color: 'red',
    marginVertical: 16,
  },
  Text: {
    fontSize: 20,
    color: 'black',
  },
  smallText: {
    fontSize: 15,
    color: 'black',
  },

});
 
export default App;
